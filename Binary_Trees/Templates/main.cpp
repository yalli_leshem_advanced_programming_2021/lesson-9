#include "functions.h"
#include "SimpleClass.h"
#include <iostream>

void testForDouble();
void testForChar();
void testForSimpleClass();

int main() {

	testForDouble();
	testForChar();
	testForSimpleClass();
	return 1;
}

void testForDouble()
{
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;

	system("pause");
}

void testForChar()
{
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<char>('a', 'b') << std::endl;
	std::cout << compare<char>('z', 'a') << std::endl;
	std::cout << compare<char>('a', 'a') << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	char charArr[arr_size] = { 'z', 'v', 'g', 'c', 'x' };
	bubbleSort<char>(charArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, arr_size);
	std::cout << std::endl;

	system("pause");
}

void testForSimpleClass()
{
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<SimpleClass>(SimpleClass(11), SimpleClass(25)) << std::endl;
	std::cout << compare<SimpleClass>(SimpleClass(45), SimpleClass(24)) << std::endl;
	std::cout << compare<SimpleClass>(SimpleClass(44), SimpleClass(44)) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	SimpleClass simpleClassArr[arr_size] = { SimpleClass(1000), SimpleClass(20), SimpleClass(34), SimpleClass(17), SimpleClass(50) };
	bubbleSort<SimpleClass>(simpleClassArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << simpleClassArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<SimpleClass>(simpleClassArr, arr_size);
	std::cout << std::endl;

	system("pause");
}