#pragma once

#include <algorithm>
#include <iostream>

template <typename T>
int compare(T first, T second)
{
	if (first < second) // if first is smaller then second
		return 1;
	else if (first == second) // if there equal
		return 0;
	else // if first is bigger then second
		return -1;
}

template <typename T> 
void bubbleSort(T arr[], int n)
{
	const int FIRST_IS_BIGGER = -1;

	for (int i = 0; i < n - 1; i++)
	{
		// Last i elements are already in place  
		for (int j = 0; j < n - i - 1; j++)
		{
			if (FIRST_IS_BIGGER == compare(arr[j], arr[j + 1]))
			{
				 std::swap(arr[j], arr[j + 1]);
			}
		}
	}
}

template <typename T>
void printArray(T arr[], int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}