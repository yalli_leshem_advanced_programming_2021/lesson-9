#include "SimpleClass.h"

std::ostream& operator<<(std::ostream& out, const SimpleClass& obj)
{
	out << obj._member;
	return out;
}

bool SimpleClass::operator<(const SimpleClass& other)
{
	return this->_member < other._member;
}

bool SimpleClass::operator==(const SimpleClass& other)
{
	return this->_member == other._member;
}
