#pragma once

#include <iostream>

class SimpleClass
{
public:
	int _member;
	SimpleClass(const int num) : _member(num)
	{}

	bool operator<(const SimpleClass& other);
	bool operator==(const SimpleClass& other);
	friend std::ostream& operator << (std::ostream& out, const SimpleClass& obj);
};

