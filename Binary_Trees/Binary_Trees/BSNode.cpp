#include "BSNode.h"

/// <summary>
/// default constructor 
/// </summary>
/// <param name="data">the roots data</param>
BSNode::BSNode(std::string data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}

/// <summary>
/// deep copy constructor
/// *uses operator= 
/// </summary>
/// <param name="other">the object to copy from</param>
BSNode::BSNode(const BSNode& other)
{
	*this = other;
}

/// <summary>
/// destructor
/// </summary>
BSNode::~BSNode()
{
	if (this->getLeft() != nullptr) // if there is still another branch n the left
	{
		delete this->_left;
		this->_left = nullptr;
	}
	if (this->getRight() != nullptr) // if there is another branch in the right
	{
		delete this->_right;
		this->_right = nullptr;
	}
}

/// <summary>
/// inserts a value in to the tree
/// (calls the recursive function to do so)
/// </summary>
/// <param name="value">the value we want to insert</param>
void BSNode::insert(std::string value)
{
	this->insert(value, this);
}

/// <summary>
/// deep copy operator (uses recursive copy function)
/// </summary>
/// <param name="other">the object we want to copy from</param>
/// <returns>a reference to the new object</returns>
BSNode& BSNode::operator=(const BSNode& other)
{
	if (this == &other) // tries to copy the object itself
		return *this;

	// release old memory
	delete this->_left;
	delete this->_right;

	// shallow copy fields
	this->_data = other.getData();
	this->_count = other._count;

	// deep copy
	copyRoot(this, &other);

	return *this;
}

/// <summary>
/// if node does not have a left and a right child it's a leaf
/// </summary>
/// <returns>true if node is a leaf</returns>
bool BSNode::isLeaf() const
{
	return (this->_left == nullptr && this->_right == nullptr);
}

/// <summary>
/// getter for the data
/// </summary>
/// <returns>the data of the wanted object (if pointer is invalid return nullptr)</returns>
std::string BSNode::getData() const
{
	if(this == nullptr) // if invalid pointer
		return nullptr;

	return this->_data;
}

/// <summary>
/// getter for the left child node
/// </summary>
/// <returns>the left child (or nullptr if this is an invalid pointer)</returns>
BSNode* BSNode::getLeft() const
{
	if(this == nullptr) // if invalid pointer
		return nullptr;

	return this->_left;
}

/// <summary>
/// getter for the right child node
/// </summary>
/// <returns>the right child (or nullptr if this is an invalid pointer)</returns>
BSNode* BSNode::getRight() const
{
	if (this == nullptr) // if invalid pointer
		return nullptr;

	return this->_right;
}

/// <summary>
/// public search function, checks if a value is in the tree (calls recursive search function to do so)
/// </summary>
/// <param name="val">the value we want to search for</param>
/// <returns>true if value was found, else false</returns>
bool BSNode::search(std::string val) const
{
	return this->search(val, this);
}

/// <summary>
/// get the height of the tree (the longest branch in the tree)
/// (uses recursive function to do so)
/// </summary>
/// <returns>the height of the tree</returns>
int BSNode::getHeight() const
{
	return this->getHeight(this);
}

/// <summary>
/// get the depth of the node (the distance between the current node and the root)
/// </summary>
/// <param name="root">the father of all childes, the main node</param>
/// <returns>the depth of the node</returns>
int BSNode::getDepth(const BSNode& root) const
{
	if (this == nullptr) // if empty
		return -1;
	else if (&root == nullptr) // if root pointer is empty
		return -1;

	if (root.search(this->getData())) // if found 
	{
		return this->getCurrNodeDistFromInputNode(&root);
	}
	else // if not found
	{
		return -1;
	}
}

/// <summary>
/// public printing function, prints all the nodes in the tree in in-order format (meaning left-root-right) 
/// (uses recursive printing function to do so)
/// </summary>
void BSNode::printNodes() const
{
	this->inOrderPrint(this);
}

/// <summary>
/// helper for getDepth function (recursive function)
/// </summary>
/// <param name="node">the child of the root node</param>
/// <returns>the depth of the node</returns>
int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	if (this == nullptr) // if empty
		return -1;
	if (node->getData().compare(this->getData()) == 0) // if node is found
	{
		return 0;
	}
	if (node->getRight()->search(this->getData())) // if the value is in the right node
	{
		return this->getCurrNodeDistFromInputNode(node->getRight()) + 1; // returns the dist from the right node +1
	}
	else // if the value is less then the data (need to go to the left node)
	{
		return this->getCurrNodeDistFromInputNode(node->getLeft()) + 1; // returns the dist from the left node +1
	}
}

/// <summary>
/// recursive function to copy all nodes in the tree (except for the root) 
/// </summary>
/// <param name="thisRoot">the current root (can be a sub root)</param>
/// <param name="otherRoot">the current root we want to copy (can be a sub root)</param>
void BSNode::copyRoot(BSNode* thisRoot, const BSNode* otherRoot)
{
	if (otherRoot->getLeft() != nullptr) // if there is a node in the left 
	{
		thisRoot->_left = new BSNode(otherRoot->getLeft()->getData()); // copy the left node
		thisRoot->_left->_count = otherRoot->getLeft()->_count;
		copyRoot(thisRoot->getLeft(), otherRoot->getLeft()); 
	}
	if (otherRoot->getRight() != nullptr) // if there is a node in the right
	{
		thisRoot->_right = new BSNode(otherRoot->getRight()->getData()); // copy the right node
		thisRoot->_right->_count = otherRoot->getRight()->_count;
		copyRoot(thisRoot->getRight(), otherRoot->getRight());
	}
	return;
}

/// <summary>
/// recursive function to insert a new value
/// </summary>
/// <param name="val">the value we want to insert</param>
/// <param name="node">the current node we are at</param>
void BSNode::insert(const std::string val, BSNode* node)
{
	if (val.compare(node->getData()) == 0) // if the value is equal to the one inside this node (need to inc _counter)
	{
		node->_count++;
		return;
	}
	else if (val.compare(node->getData()) > 0) // if the value is bigger then the data (need to go to the right node)
	{
		if (node->getRight() == nullptr) // if next node is empty 
		{
			node->_right = new BSNode(val);
			return;
		}
		return this->insert(val, node->_right);
	}
	else if (val.compare(node->getData()) < 0) // if the value is less then the data (need to go to the left node)
	{
		if (node->getLeft() == nullptr) // if next node is empty 
		{
			node->_left = new BSNode(val);
			return;
		}
		return this->insert(val, node->_left);
	}
}

/// <summary>
/// recursive function to search for a value in the tree
/// </summary>
/// <param name="val">the value we want to search for</param>
/// <param name="node">the current node we are at</param>
/// <returns>true if the value was found, if not then false </returns>
bool BSNode::search(const std::string val, const BSNode* node) const
{
	if (node == nullptr)
		return false;

	else if (val.compare(node->getData()) == 0) // if found
		return true;

	else if (node->isLeaf()) // if last node
		return false;

	else if (val.compare(node->getData()) > 0) // if the value is bigger then the data (need to go to the right node)
		return this->search(val, node->getRight());

	else if (val.compare(node->getData()) < 0) // if the value is less then the data (need to go to the left node)
		return this->search(val, node->getLeft());

	else
		return false;
}

/// <summary>
/// recursive function to get the height of the tree (more info in the public getHieght function)
/// </summary>
/// <param name="node">the current node we are at</param>
/// <returns>the height of the tree</returns>
int BSNode::getHeight(const BSNode* node) const
{
	if (node == nullptr) // if we got to the end, start counting
	{
		return 0;
	}
	else // if there are two nodes after this one
	{
		return (std::max(node->getHeight(node->getLeft()), node->getHeight(node->getRight())) + 1); // returns the higher node plus 1
	}
}

/// <summary>
/// recursive function to print in-order (meaning left-root-right) 
/// </summary>
/// <param name="node">the current node we are at</param>
void BSNode::inOrderPrint(const BSNode* node) const
{
	if (node == nullptr)
		return;

	// in order print - print the left, then the root, then the right
	if (node->getLeft() != nullptr) // if there is a left child, this is a root, go to the left child first
	{
		this->inOrderPrint(node->getLeft());
	}

	// now, after we did the left side, we need to print the root (this node)
	std::cout << node->getData() << " " << node->_count << std::endl;

	if (node->getRight() != nullptr) // after we printed the left child and the root we need to go to the right child
	{
		this->inOrderPrint(node->getRight());

	}
}
