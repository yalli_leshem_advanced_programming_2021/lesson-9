#pragma once

#include <string>
#include <iostream>
#include <algorithm>

class BSNode
{
public:
	BSNode(std::string data);
	BSNode(const BSNode& other);

	~BSNode();

	void insert(std::string value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	std::string getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(std::string val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;

	void printNodes() const; //for question 1 part C

private:
	std::string _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth

	void copyRoot(BSNode* thisRoot, const BSNode* otherRoot); // copys only sub roots (root needs to be copied separately

	void insert(const std::string val, BSNode* node); // recursive function of insert

	bool search(const std::string val, const BSNode* node) const; // recursive function of search

	int getHeight(const BSNode* node) const; // recursive function of getHeight

	void inOrderPrint(const BSNode* node) const; // recursive function for printing


};