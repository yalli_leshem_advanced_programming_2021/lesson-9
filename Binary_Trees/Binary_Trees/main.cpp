#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"

void autoCheck(); // auto checks stuff, no need to input


void printAndShow(const BSNode* bs); // prints and runs the exe file

int main()
{
	autoCheck();
	
	return 0;
}

void autoCheck()
{

	// checks inserts and doubles
	BSNode* bs = new BSNode("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("7");
	bs->insert("3");
	bs->insert("9");
	bs->insert("1");
	bs->insert("3");
	bs->insert("9");
	bs->insert("5");
	bs->insert("7");
	bs->insert("6");

	std::cout << "bs nodes:" << std::endl;

	printAndShow(bs); // check printing node and see of inserted properly

	std::cout << "bs Tree height: " << bs->getHeight() << std::endl; // checks height of tree
	std::cout << "invalid depth (and invalid pointer): " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << std::endl; // check what happens when you use an invalid pointer
	std::cout << "depth of node with 3 depth(valid): " << bs->getRight()->getLeft()->getDepth(*bs) << std::endl;

	// check operator=
	BSNode bss = *bs;
	std::cout << "(used operator= on bss) bss nodes: " << std::endl;
	printAndShow(&bss);

	// check inserts on bss 
	bss.insert("2");
	bss.insert("4");
	bss.insert("8");
	bss.insert("7");
	bss.insert("7");
	bss.insert("3");
	bss.insert("3");
	bss.insert("5");
	bss.insert("5");
	bss.insert("9");
	bss.insert("9");
	bss.insert("6");

	std::cout << "(after inserting more values to bss) bss nodes: " << std::endl;
	printAndShow(&bss);

	std::cout << "(check if bs didnt change) bs nodes:" << std::endl;
	printAndShow(bs); // check printing node and see of inserted properly

	*bs = bss;
	std::cout << "did: '*bs = bss' check that bs was copied properly:" << std::endl;
	printAndShow(bs);

	// insert stuff to bs, check if bss didnt change
	bs->insert("0");
	bs->insert("0");
	bs->insert("0");
	bs->insert("0");
	std::cout << "inserted 0(4 times) to bs, check if inserted properly:" << std::endl;
	printAndShow(bs);

	std::cout << "(after inserting more values to bs, ccheck if bss changed) bss nodes: " << std::endl;
	bss.printNodes();

	system("pause");
	delete bs;
}

void printAndShow(const BSNode* bs)
{
	bs->printNodes();

	std::string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);
	system("BinaryTree.exe");
	remove(textTree.c_str());
}