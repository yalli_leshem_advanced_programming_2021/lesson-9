#include "TBSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	const unsigned MAX_INT = 100;
	const unsigned LEN = 15;
	const unsigned UPPER_NUM_OF_CHARS = 10;
	const unsigned MIN_CHAR = 97;
	const unsigned CHAR_VALUE_RANGE = 26;

	int* intArr = new int[LEN];
	std::string* stringArr = new std::string[LEN];

	srand(time(NULL));

	for (unsigned i = 0; i < LEN; i++)
	{
		intArr[i] = rand() % MAX_INT;

		unsigned numOfChars = rand() % UPPER_NUM_OF_CHARS;
		for (unsigned j = 0; j < numOfChars; j++)
		{
			stringArr[i] += rand() % CHAR_VALUE_RANGE + MIN_CHAR;
		}
	}

	std::cout << "Int array values:" << std::endl;
	for (unsigned i = 0; i < LEN; i++)
	{
		std::cout << intArr[i] << " ";
	}
	std::cout << std::endl;

	std::cout << "String array values:" << std::endl;
	for (unsigned i = 0; i < LEN; i++)
	{
		std::cout << stringArr[i] << " ";
	}
	std::cout << std::endl;

	TBSNode<int>* intTree = new TBSNode<int>(intArr[0]);
	TBSNode<std::string>* stringTree = new TBSNode<std::string>(stringArr[0]);

	for (unsigned i = 1; i < LEN; i++)
	{
		intTree->insert(intArr[i]);
		stringTree->insert(stringArr[i]);
	}

	std::cout << "Int tree values:" << std::endl;
	intTree->printNodes();
	std::cout << std::endl;

	std::cout << "String tree values:" << std::endl;
	stringTree->printNodes();
	std::cout << std::endl;

	delete[] intArr;
	delete[] stringArr;
	delete intTree;
	delete stringTree;
	return 0;
}